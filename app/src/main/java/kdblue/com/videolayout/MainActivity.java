package kdblue.com.videolayout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {



    ImageView playicon;
    LinearLayout ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        playicon=(ImageView)findViewById(R.id.imageView);
        ll=(LinearLayout)findViewById(R.id.full_video_mode);


        playicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                playicon.setVisibility(View.GONE);
                ll.setVisibility(View.VISIBLE);

                getFragmentManager().beginTransaction().add(R.id.full_video_mode,new Video_Fragment()).commit();

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        playicon.setVisibility(View.VISIBLE);
        ll.setVisibility(View.GONE);
    }
}
